# Preisschild NixOS Configs

## Inspired by https://github.com/srid/nix-config

## On NixOS
```
sudo nix-channel --add https://nixos.org/channels/nixos-unstable nixos
sudo nix-channel --add https://github.com/NixOS/nixos-hardware/archive/master.tar.gz nixos-hardware
sudo nix-channel --add https://github.com/rycee/home-manager/archive/master.tar.gz home-manager
sudo nix-channel --update

ssh-keygen  # add to gitlab

nix-shell -p git -p vim
...
git clone git@gitlab.com:Preisschild/nix-config.git $HOME/nix-config
cd $HOME/nix-config

# First, review ./machine/<hostname>/.nix
sudo mv /etc/nixos/configuration.nix /tmp/
sudo ln -s $(pwd)/machine/<hostname>/default.nix /etc/nixos/configuration.nix
```

