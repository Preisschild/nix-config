{
  scan_timeout        = 10;
  character.symbol    = "➜";

  aws.symbol          = " ";
  battery = {
    full_symbol        = "";
    charging_symbol    = "";
    discharding_symbol = "";
  };
  conda.symbol        = " ";
  git_branch.symbol   = " ";
  golang.symbol       = " ";
  hg_branch.symbol    = " ";
  java.symbol         = " ";
  memory_usage.symbol = " ";
  nodejs.symbol       = " ";
  package.symbol      = " ";
  php.symbol          = " ";
  python.symbol       = " ";
  ruby.symbol         = " ";
  rust.symbol         = " ";
}
