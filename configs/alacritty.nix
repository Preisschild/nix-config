# https://hugoreeves.com/posts/2019/nix-home/
{
  env = {
    "TERM" = "xterm-256color";
  };

  window = {
    dimensions = {
      columns = 0;
    };
  
    padding = {
      x = 2;
      y = 2;
    };

    decorations = "none";
  };
  
  scrolling = {
    history = 50000;
  };
  
  font = {
    normal = {
      family = "FiraCode Nerd Font";
      style = "Regular";
    };
    bold = {
      family = "FiraCode Nerd Font";
      style = "Bold";
    };
    italic = {
      family = "FiraCode Nerd Font";
      style = "Italic";
      size = 11.0;
    };
  };

  draw_bold_text_with_bright_colors = true;

  # Colors (Snazzy)
  colors = {
    primary = {
      background = "0x282a36";
      foreground = "0xeff0eb";
    };
  
    normal = {
      black   = "0x282a36";
      red     = "0xff5c57";
      green   = "0x5af78e";
      yellow  = "0xf3f99d";
      blue    = "0x57c7ff";
      magenta = "0xff6ac1";
      cyan    = "0x9aedfe";
      white   = "0xf1f1f0";
    };

    bright = {
      black   = "0x686868";
      red     = "0xf25e48";
      green   = "0x57e87d";
      yellow  = "0xfef8a4";
      blue    = "0x4ad4f8";
      magenta = "0xff61ce";
      cyan    = "0xaee4f8";
      white   = "0xeff0eb";
    };
  };

  background_opacity = 0.9;
  
  # Allow terminal applications to change Alacritty"s window title.
  dynamic_title = true;
  
  cursor = {
    style = "Beam";
  };

  live_config_reload = true;
  
  shell = {
    program = "bash";
    args = [ "--login" ];
  };
   
  key_bindings = [
    { key = "V";        mods = "Control|Shift"; action = "Paste";                   }
    { key = "C";        mods = "Control|Shift"; action = "Copy";                    }
    { key = "Paste";                   action = "Paste";                            }
    { key = "Copy";                    action = "Copy";                             }
  ];
}
