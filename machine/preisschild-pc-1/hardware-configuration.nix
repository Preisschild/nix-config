{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot.initrd.availableKernelModules = [ "nvme" "ahci" "xhci_pci" "usbhid" "usb_storage" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = [ ]; 

  hardware.opengl.driSupport32Bit = true;
  hardware.opengl.extraPackages32 = with pkgs.pkgsi686Linux; [ libva ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/e6cf7e2d-c9ea-48fb-9fe8-7c1dd0222aaa";
      fsType = "ext4";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/8DE2-928C";
      fsType = "vfat";
    };

    fileSystems."/mnt/Games" = {
      device="/dev/disk/by-uuid/9ed62fa4-382f-4420-8985-84a79b4019dd";
      fsType = "ext4";
    };

  swapDevices = [ ];

}
