{ config, pkgs, lib, ... }:

let
  nur = import (fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {};
in
{
  imports =
    [
      <home-manager/nixos>
      ./hardware-configuration.nix
    ];


  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Network settings
  networking.hostName = "preisschild-pc-1";
  networking.useDHCP = true;

  # Locality settings
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "de";
  };
  time.timeZone = "Europe/Vienna";

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Firewall Settings
  networking.firewall = {
    # Open ports in the firewall
    ## TCP
    allowedTCPPorts = [ 
      22 # SSH
    ];
    allowedTCPPortRanges = [
      { from = 1714; to = 1764; }
    ];
    ## UDP
    allowedUDPPorts = [ 
    ];
    allowedUDPPortRanges = [
      { from = 1714; to = 1764; }
    ];

    # Allow Ping
    allowPing = true;

    # Log Refused Connections
    logRefusedConnections = true;
  };

  # Enable sound.
  sound.enable = true;
  nixpkgs.config.pulseaudio = true;
  hardware.pulseaudio = {
    enable = true;
    support32Bit = true;
  };

  # Enable the X11 windowing system. (for now)
  services.xserver.enable = true;
  services.xserver.layout = "de";



  # Users
  users.users.preisschild = {
    isNormalUser = true;
    extraGroups = [ "wheel" "audio" "docker" "scanner" ];
    shell = pkgs.zsh;
  };

  # NixOS Version of the initial Install
  system.stateVersion = "20.09";

  # Fonts
  fonts.fonts = with pkgs; [
    nerdfonts
  ];

  # Program specific settings

  ## Shell
  programs.zsh.enable = true;
  programs.zsh.ohMyZsh = {
    enable = true;
    plugins = ["git" "sudo" "docker" "kubectl"];
  };

  # Security
  security.sudo.wheelNeedsPassword = false;

  # Install Packages
  environment.systemPackages = with pkgs; [
    wget home-manager git rcm ctags nodejs killall pavucontrol p11-kit vim sane-backends lm_sensors
  ];

  # Flatpak
  xdg.portal.enable = true;
  services.flatpak.enable = true;

  # NTP
  services.ntp.enable = true;

  # Docker
  virtualisation.docker.enable = true;

  # Sway
  programs.sway.enable = true;

  services.xserver.displayManager = {

    # defaultSession = "sway";
    lightdm = {
      enable = true;
#       greeters.webkit2 = {
#         enable = true;
#         webkitTheme = fetchTarball {
#           url = "https://github.com/Litarvan/lightdm-webkit-theme-litarvan/releases/download/v3.1.0/lightdm-webkit-theme-litarvan-3.1.0.tar.gz";
#           sha256 = "1gn8xn4cn0ca2s9m5lzbg1wjp74mjlx4vrfww4axd2va5x1pqyvq";
#         };
#       };
    };
  };

  # Sane
  hardware.sane = {
    enable = true;
    extraBackends = [ pkgs.hplipWithPlugin ];
    netConf = "stroeger-rpi-schrank";
  };
  nixpkgs.config = {
    allowUnfree = true;
  };

  powerManagement = {
    enable = true;
    cpuFreqGovernor = "ondemand";
  };

}



