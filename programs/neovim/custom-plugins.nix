{ pkgs, ...}:

{
  vim-snazzy = pkgs.vimUtils.buildVimPlugin {
  	name = "vim-snazzy";
  	src = pkgs.fetchFromGitHub {
  	  owner = "connorholyday";
  	  repo = "vim-snazzy";
  	  rev = "d979964b4dc0d6860f0803696c348c5a912afb9e";
  	  sha256 = "0a9m48dv05fp8vc0949gr5yhj8wp09p7f125waazwd5ax8f591p9";
  	};
  };
  vim-tmuxline = pkgs.vimUtils.buildVimPlugin {
    name = "vim-tmuxline";
    src = pkgs.fetchFromGitHub {
      owner = "edkolev";
      repo = "tmuxline.vim";
      rev = "7001ab359f2bc699b7000c297a0d9e9a897b70cf";
      sha256 = "13d87zxpdzryal5dkircc0sm88mwwq7f5n4j3jn9f09fmg9siifb";
    };
  };
}
