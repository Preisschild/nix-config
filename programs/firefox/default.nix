{ config, pkgs, lib, ... }:

{
  programs.firefox = {
    enable = true;

    package = pkgs.firefox-wayland;
    
    extensions = lib.mkIf config.programs.firefox.enable (
      with pkgs.nur.repos.rycee.firefox-addons; 
      [
        https-everywhere
        privacy-badger
        ublock-origin
        bitwarden
        decentraleyes
        i-dont-care-about-cookies
        old-reddit-redirect
        reddit-enhancement-suite
        stylus
      ] ++ builtins.attrValues (import ./custom-extensions.nix {
        inherit (pkgs) stdenv fetchurl;
        buildFirefoxXpiAddon =
          pkgs.nur.repos.rycee.firefox-addons.buildFirefoxXpiAddon;
        })
      );


    profiles = {
      "default" = {
        id = 0;
        settings = {
          "browser.startup.homepage" = "https://nixos.org";
          "services.sync.engine.addons" = false;
          "services.sync.engine.passwords" = false;
          "services.sync.engine.prefs" =  false;
          "signon.formlessCapture.enabled" = false;
          "signon.rememberSignons.enabled" = false;
          "signon.rememberSignons" = false;
          "findbar.highlightAll" = true;
          "browser.uiCustomization.state" = ''
            {"placements":{"widget-overflow-fixed-list":["jid1-bofifl9vbdl2zq_jetpack-browser-action","_7a7a4a92-a2a0-41d1-9fd7-1e92480d612d_-browser-action","_74145f27-f039-47ce-a470-a662b129930a_-browser-action","jid1-mnnxcxisbpnsxq_jetpack-browser-action","jid1-kkzogwgsw3ao4q_jetpack-browser-action"],"nav-bar":["back-button","forward-button","stop-reload-button","home-button","urlbar-container","downloads-button","library-button","https-everywhere_eff_org-browser-action","ublock0_raymondhill_net-browser-action","_446900e4-71c2-419f-a6a7-df9c091e268b_-browser-action"],"toolbar-menubar":["menubar-items"],"TabsToolbar":["tabbrowser-tabs","new-tab-button","alltabs-button"],"PersonalToolbar":["personal-bookmarks"]},"seen":["developer-button","https-everywhere_eff_org-browser-action","jid1-bofifl9vbdl2zq_jetpack-browser-action","jid1-kkzogwgsw3ao4q_jetpack-browser-action","jid1-mnnxcxisbpnsxq_jetpack-browser-action","ublock0_raymondhill_net-browser-action","_446900e4-71c2-419f-a6a7-df9c091e268b_-browser-action","_74145f27-f039-47ce-a470-a662b129930a_-browser-action","_7a7a4a92-a2a0-41d1-9fd7-1e92480d612d_-browser-action"],"dirtyAreaCache":["nav-bar","widget-overflow-fixed-list"],"currentVersion":16,"newElementCount":4}
            '';
          "extensions.update.enabled" = false;
          "devtools.theme" = "dark";
          "extensions.activeThemeID" = "{467c0f66-2faa-4636-93bb-9a5eda23389f}";
        };
        userChrome = builtins.readFile ./userChrome.css;
      };
    };
  };
}

