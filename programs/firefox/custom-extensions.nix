{ buildFirefoxXpiAddon, fetchurl, stdenv }: 

{
  "SolarizeFox" = buildFirefoxXpiAddon rec {
    pname = "SolarizeFox";
    version = "2.3.6";
    addonId = "{467c0f66-2faa-4636-93bb-9a5eda23389f}";
    sha256 = "0g9b6hl1wnpnrpapck4wgnr3g0p0ga8djfbcqqgghbifcwrysyj9";
    url = "https://addons.mozilla.org/firefox/downloads/file/3517030/solarize_fox-${version}-an+fx.xpi";
    meta = with stdenv.lib; {
      description = "Solarized Firefox";
    };
  };

  "clearurls" = buildFirefoxXpiAddon rec {
    pname = "clearurls";
    version = "1.18.1";
    addonId = "{74145f27-f039-47ce-a470-a662b129930a}";
    sha256 = "016sxv6bb51752pi15qdw82pc5wzv7rv6y6b4l34sa8dg6x19qr6";
    url = "https://addons.mozilla.org/firefox/downloads/file/3586570/clearurls-${version}-an+fx.xpi";
    meta = with stdenv.lib; {
      description = "Remove tracking elements from URLs.";
      license = licenses.gpl3;
      platforms = platforms.all;
      homepage = "https://gitlab.com/KevinRoebert/ClearUrls";
    };
  };



}

