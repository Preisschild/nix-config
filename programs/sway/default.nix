{ config, pkgs, ... }:
let
cfg = config.wayland.windowManager.sway.config;
mod = cfg.modifier;
in
{
  enable = true;
  config = with pkgs; {
    modifier = "Mod4";
    bars = [
      {
        position = "top";
        statusCommand = "/dev/true";
        command = "${waybar}/bin/waybar";
      }
    ];
    input."*" = {
      xkb_layout = "de";
    };
    terminal = "${alacritty}/bin/alacritty";
    output = {
      "*" = {
        bg = "/home/preisschild/Pictures/616ot.png fill";
      };
      DP-1 = {
        pos = "1980 529";
        res = "1920x1200";
      };
      HDMI-A-1 = {
        pos = "780 0";
        res = "1920x1200";
        transform = "270";
      };
      HDMI-A-2 = {
        pos = "3900 529";
        res = "1920x1080";
      };
    };
    menu = "${dmenu}/bin/dmenu_path | ${wofi}/bin/wofi --show drun -i | ${findutils}/bin/xargs swaymsg exec --¬";
    keybindings = 
      lib.mkOptionDefault {
      "${mod}+Print" = ''exec IMG=~/Pictures/$(date +%Y-%m-%d_%H-%m-%s).png && grim -g "$(slurp)" $IMG && wl-copy < $IMG'';
      };
  };

}
