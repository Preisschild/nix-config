{ config, pkgs, options, lib, attrsets, ... }:
{
  imports = [
    ./programs/firefox/default.nix
  ];


  home.packages = with pkgs; [
    # Utils
    file
    imagemagick
    pcmanfm
    pciutils
    binutils
    neofetch
    glxinfo
    fzf
    unzip
    ldns
    htop
    fortune
    tdesktop
    vim
    libnotify
    kubectl
    kubectx
    mpv
    sway
    nmap
    telnet
    openssl
    flatpak-builder
    netperf
    ipmitool
    lshw
    nushell
    kubernetes-helm
    httpie
    go
    nixfmt
    glow
    wine
    ranger
    links
    qutebrowser
    hadolint
    # xorg.xrandr
    # xwayland
    jre8
    tealdeer
    xdg_utils
    khal
    vdirsyncer
    dmidecode
    audacity
    figlet
    lolcat
    cowsay
    jq
    pkg-config
    libusb1
    libusb-compat-0_1
    usbutils
    simple-scan
    phoronix-test-suite
    terraform
    qpdf
    jadx
    vlc

    # Haskell Packages
    cabal-install
    haskellPackages.ghc


    # Python Packages
    python37
    python37Packages.pip
    python37Packages.ansible
    python37Packages.ansible-lint
    python38Packages.binwalk
    python37Packages.youtube-dl
    python37Packages.pyudev
    python37Packages.wakeonlan
    python37Packages.setuptools
    python37Packages.six
    python37Packages.poetry
    python37Packages.meson

    # Games
    multimc
    lutris-free
    steam
    xboxdrv

    # Chats
    discord
    teamspeak_client
    slack

    # Sway Related
    wofi
    waybar
    swaylock
    xwayland
    mako
    grim
    slurp
    wl-clipboard
  ];

  programs = {

    home-manager = {
      enable = true;
    };

    git = {
      package = pkgs.gitAndTools.gitFull;
      enable = true;
      userName = "Florian Ströger";
      userEmail = "florian@florianstroeger.com";
    };

    fzf = {
      enable = true;
      enableZshIntegration = true;
    };

    command-not-found.enable = true;

    tmux = {
      enable = true;
      clock24 = true;
      historyLimit = 50000;
      keyMode = "vi";
      newSession = false;
      terminal = "screen-256color";
      sensibleOnTop = true;
      tmuxinator.enable = true;
      extraConfig = ''
        set -g mouse on
        if-shell "test -f ~/.local/share/tmuxline.conf" "source ~/.local/share/tmuxline.conf"
      '';
      plugins = with pkgs; [
        tmuxPlugins.cpu
        tmuxPlugins.yank
        {
          plugin = tmuxPlugins.resurrect;
          extraConfig = "set -g @resurrect-strategy-nvim 'session'";
        }
        {
          plugin = tmuxPlugins.continuum;
          extraConfig = ''
            set -g @continuum-restore 'on'
            set -g @continuum-save-interval '60' # minutes
          '';
        }
      ];
    };

    zsh = {
      enable = true;
      autocd = true;
      enableAutosuggestions = true;
      plugins = with pkgs; [
        {
          file = "share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh";
          name = "zsh-syntax-highlighting";
          src  = zsh-syntax-highlighting;
        }
      ];

      oh-my-zsh = {
        enable = true;
        plugins = [
          "git"
          "sudo"
          "docker"
          "kubectl"
          "ansible"
          "catimg"
          "colored-man-pages"
          "colorize"
          "docker"
          "docker-compose"
          "fzf"
          "golang"
          "helm"
          "httpie"
        ];
      };
    };

    alacritty = {
      enable = true;
      settings = lib.attrsets.recursiveUpdate (import ./configs/alacritty.nix) {
        shell.program = "bash";
        # shell.args = [ "-l" "-c" "tmux attach || tmux" ];
        shell.args = [ "-l" "-c" "tmux" ];

      };
    };

    starship = {
      enable = true;
      enableZshIntegration = true;
      settings = (import ./configs/starship.nix);
    };

    neovim = (import ./programs/neovim/default.nix { pkgs = pkgs; });

  };

  nixpkgs.config = {
    pulseaudio = true;
    allowUnfree = true;
    allowBroken = true;
    permittedInsecurePackages = [
      "p7zip-16.02"
    ];

    xdg = {
      enable = true;
      portal = {
        enable = true;
        extraPortals = with pkgs; [
          xdg-desktop-portal-wlr
        ];
      };
    };
    
    # Nix User Repository
    packageOverrides = pkgs: {
      nur = import(builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
        inherit pkgs;
      };
    };
  };

  services = {
    redshift = {
      enable = true;
      package = pkgs.redshift-wlr;
      provider = "manual";
      latitude = "48.208176";
      longitude = "16.373819";
      temperature = {
        day = 6500;
        night = 3000;
      };
    };
    
    kdeconnect = {
      enable = true;
      indicator = true;
    };
  };

  home.sessionVariables = {
    EDITOR = "vim";
    PYTHONPATH = ''$(find -L ~/.nix-profile -path "*/python*/site-packages" -type d | tr '\n' ':')$PYTHONPATH'';
  };

  xdg = {
    configFile = {
      "wofi/config".text = builtins.readFile ./configs/wofi/config;
      "wofi/selenized-dark.css".text = builtins.readFile ./configs/wofi/selenized-dark.css;
    };
  };

  wayland.windowManager.sway = (import ./programs/sway/default.nix { pkgs = pkgs; config = config; });

 
}


